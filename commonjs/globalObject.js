import * as globalVars from './globalVariables.js';
import {createSecurityProgressBar} from './sectionLogRegForms/securityProgressBar.js';

// object for creating a group (LOGIN) of input and label
export const dataInputLabelLogin = {
    dataInput: {
        inputType         : 'text',
        inputName         : 'userName',
        inputClassName    : 'input-login',
        inputAutocomplete : 'off',
        inputRequired     : 'required',
    },

    dataLabel: {
        labelValue : 'Username',
        labelClass : 'label-login',
        labelFor   : 'userName',
    },
}

// object for creating a group (PASSWORD) of input and label
export const dataInputLabelPass = {
    dataInput: {
        inputType         : 'password',
        inputName         : 'password',
        inputClassName    : 'input-password',
        inputAutocomplete : 'off',
        inputRequired     : 'required',
    },

    dataLabel: {
        labelValue : 'Password',
        // labelClass : 'label-registring',
        labelClass : 'label-pass',
        labelFor   : 'password',
    },
}

// object for creating a group (repeatPASSWORD) of input and label
export const dataInputLabelRepeatPass = {
    dataInput: {
        inputType         : 'password',
        inputName         : 'repeat-password',
        inputClassName    : 'input-repeat-password',
        inputAutocomplete : 'off',
        inputRequired     : 'required',
    },

    dataLabel: {
        labelValue : 'Repeat Password',
        labelClass : 'label-repeat-pass',
        labelFor   : 'repeatPassword',
    },
}

export const dataInputLabelShowHidePass = {
    dataInput: {
        inputType         : 'checkbox',
        inputName         : 'show-password',
        inputId           : 'show-password',
        inputClassName    : 'input-show-password',
        inputAutocomplete : '',
        inputRequired     : '',
    },

    dataLabel: {
        labelValue : ' Show password',
        labelClass : 'label-for-show-password',
        labelFor   : 'show-password',
    },
}