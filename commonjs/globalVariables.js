//common selectors
export const root   = document.querySelector('#root');
export const logOut = document.querySelector('.user-nav__item:last-child');

// common elements
export const header = document.createElement('header');
export const img    = document.createElement('img');

// a + img 
export const logoSrc    = './img/LogoSuperGameTechnology.svg';
export const logoAlt    = 'Logo SupperGameTechnology';
export const logoWidth  = 180;
export const aClassLogo = 'logo';

// common vars
export const divWrapper    = 'log-reg-form__wrapper';
export const actionForm    = 'index.html';
export const methodForm    = 'POST';
export const divLogRegForm = 'log-reg-form';
export const buttonClass   = 'button-form';

// variables for registration form
export const regH2         = 'Registration';
export const pHeadingReg   = 'If you want to play in our super game, please fill in this form to create an account.';
export const buttonTextReg = 'REGISTER';
export const textPLink     = 'Already have an account? ';
export const classPLink    = 'sign-in-up-link';
export const textLink      = 'Sign in';
export const classLink     = 'to_login';
export const hrefLink      = '#tologin';

// variables for login form
export const logH2         = 'Log in';
export const pHeadingLog   = 'Please log in if you want to play';
export const buttonTextLog = 'LOGIN';

export const textLabelsLog = [
    '* empty field *',
    '* this user isn\'t found *',
    '* wrong pass *',
]