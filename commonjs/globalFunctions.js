export class CreateElements {

    // create element <h2>
    h2 = (textH2 = '') => {

        const h2 = document.createElement('h2');

        h2.innerText = textH2;

        return h2;
    }

    // create element <p>
    p = (textP, classP) => {

        const p = document.createElement('p');

        textP  ? p.innerText = textP : p.innerText = 'Text paragraph';
        classP ? p.classList.add( classP ) : classP = null;

        return p;
    }

    // create element <a>
    a = (textLink, linkHref, className) => {

        const a = document.createElement('a');

        a.innerText = textLink;
        a.href      = linkHref;
        a.classList.add( className );

        return a;
    }

    // create element <div>
    div = (className, idName) => {

        const div = document.createElement('div');

        idName    ? div.id = idName : idName = null;
        className ? div.classList.add(className) : className = null;

        return div;
    }

    // create element <form>
    form = (actionName, methodName) => {

        const form = document.createElement('form');

        actionName ? form.action = actionName : actionName = null;
        methodName ? form.method = methodName : methodName = null;

        return form;
    }

    // // create element <input>
    input = dataInput => {

        const input = document.createElement('input');

        input.type         = dataInput.inputType;
        input.name         = dataInput.inputName;
        input.autocomplete = dataInput.inputAutocomplete;
        input.required     = dataInput.inputRequired;
        dataInput.inputId ? input.id = dataInput.inputId : dataInput.inputId = null;
        input.classList.add( dataInput.inputClassName );
        
        return input;
    }

    // create element <label>
    label = dataLabel => {

        const label = document.createElement('label');
    
        label.innerText = dataLabel.labelValue;
        label.classList = dataLabel.labelClass;
        label.setAttribute( 'for', dataLabel.labelFor )
    
        return label;
    }

    // create element <button>
    button = (textButton, className) => {
    
        const button = document.createElement('button');
    
        button.innerText = textButton;
        button.type = 'submit';
        className ? button.classList.add(className) : className = null;
    
        return button;
    }

    // create element <a>
    a = (textLink, linkHref, className) => {

        const a = document.createElement('a');

        a.innerText = textLink;
        a.href      = linkHref;
        a.classList.add( className );

        return a;
    }

    // create element <img>
    img = (src, alt, width) => {

        const img = document.createElement('img');

        img.setAttribute('src', src);
        img.setAttribute('alt', alt);
        img.setAttribute('width', width);

        return  img;
    }

}

export const inputLabelForWrongData = (input, label, textLabel) => {
        
    input.style.boxShadow = " red 0px 0px 5px";
    label.textContent = textLabel;
    label.style = 'color: red; font-size:14px; text-transform: uppercase;';
}
