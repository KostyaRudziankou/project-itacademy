import {createFormWrapper} from '../sectionLogRegForms/createLogRegForms.js';

export function linkListener(){
    
    let logForm = createFormWrapper.bind(this, true);
    
    const link        = document.querySelector('.to_login');
    const formWrapper = document.querySelector('.log-reg-form__wrapper');
    
    function showLogForm() {
        
        formWrapper.remove();
        
        logForm();

    }

    link.addEventListener('click', showLogForm, {once:true});
}