import {CreateElements} from '../globalFunctions.js';

export function createSecurityProgressBar() {

    const element = new CreateElements();

    const divSecurity           = element.div('security');
    const divSecurityProgessBar = element.div('security-progress-bar');

    divSecurity.append( divSecurityProgessBar );

    return divSecurity;
}