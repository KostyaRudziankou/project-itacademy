import {createFormWrapper}      from './createLogRegForms.js';
import {inputLabelForWrongData} from '../globalFunctions.js';
import {listenersLogRegForm}    from './listenersForRegLogForm.js';
import {createHelloForm}        from './helloForm.js';

export function registration(event) {
    
    event.preventDefault();
    
    const inputName   = document.querySelector('.input-login');
    const inputPass   = document.querySelector('.input-password');
    const repeatPass  = document.querySelector('.input-repeat-password');
    const labelName   = document.querySelector('.label-login');
    const labelPass   = document.querySelector('.label-pass');
    const labelRepeat = document.querySelector('.label-repeat-pass');
    const formWrapper = document.querySelector('.log-reg-form__wrapper');
    const progressBar = document.querySelector('.security-progress-bar');
    const signUp      = document.querySelector('.user-nav__item:nth-child(2)');
    
    let logForm = createFormWrapper.bind(this, true);

    const objUser = {
        name: inputName.value,
        pass: inputPass.value,
        currentMoney : 0,
        currentLevel : 0,
        experience   : 0,
    }
    
    if ( !inputName.value.trim().length > 0 || inputName.value == '' || inputName.value.length <= 2 ) {
        
        inputLabelForWrongData( inputName, labelName, '* field of "Username" is empty or too short *' );
        
    } else if ( inputPass.value.length < 4 ) { 
        
        inputLabelForWrongData( inputPass, labelPass, '* too short password, tense up! *' );;   
        
    } else if ( repeatPass.value != inputPass.value ) {
        
        inputLabelForWrongData( repeatPass, labelRepeat, '* password mismatch *' );;   
        
    } else {
        
        localStorage.setItem(inputName.value, JSON.stringify( objUser ));

        signUp.hidden = false;
        
        formWrapper.remove();
        
        createHelloForm( inputName.value, 'reg' );
        
        const helloForm   = document.querySelector('.hello-form');
        
        inputName.value = null;
        inputPass.value = null;
        repeatPass.value = null;
        progressBar.style.width = 0;

        setTimeout( () => {
            logForm();
            helloForm.remove();
        }, 6000 );

    }; 
    
    const paramForListenersForm = { inputName, inputPass, labelName, labelPass, repeatPass, labelRepeat };

    listenersLogRegForm( paramForListenersForm );

}