
export function showPass() {

    const inputRepeatPass = document.querySelector('.input-repeat-password');
    const inputshowPass   = document.querySelector('.input-show-password');
    const inputPass       = document.querySelector('.input-password');

    const arrWithInputPassword = [inputPass, inputRepeatPass];

    function showHidePass(typeChecked) {
        for (let value of arrWithInputPassword) {
            if (!value) continue;
            value.type = typeChecked;
        }
    }

    function showHidePassword() {
        inputshowPass.checked ? showHidePass('text') : showHidePass('password');
    }

  inputshowPass.addEventListener('change', showHidePassword);
}
