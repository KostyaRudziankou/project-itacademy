import {CreateElements}            from '../globalFunctions.js';
import * as globalVars             from '../globalVariables.js';
import * as globalObject           from '../globalObject.js'; 
import {registration}              from './registration.js';
import {linkListener}              from './linkListener.js';
import {login}                     from './login.js';
import {showPass}                  from './showHidePass.js';
import {indicationPass}            from './indicationPass.js';
import {createSecurityProgressBar} from './securityProgressBar.js';

export function createFormWrapper( triggerForm ) {

    const element = new CreateElements();

    // ----------------add method into the class GlobalFunctions----------------------------//

    CreateElements.prototype.inputLabelGroup = function (dataInputLabel) {

        const div = element.div('input-label-group');

        div.append( element.input( dataInputLabel.dataInput ) );
        div.append( element.label( dataInputLabel.dataLabel ) ); 
        
        return div;
    }
    
    // ----------------add method into the class GlobalFunctions----------------------------//

    const formWrapper = element.div( globalVars.divWrapper );
    const divIdForm   = element.div( globalVars.divLogRegForm );
    const form        = element.form( globalVars.actionForm, globalVars.methodForm );
    const pLink       = element.p( globalVars.textPLink, globalVars.classPLink );
    const link        = element.a( globalVars.textLink, globalVars.hrefLink, globalVars.classLink );

    const signIn      = document.querySelector('.user-nav__item:nth-child(2)');
    const signUp      = document.querySelector('.user-nav__item:nth-child(1)');
    
    pLink.append( link );

    let h2, pHeading, button;

    if(triggerForm) {

        h2       = element.h2( globalVars.logH2 );
        pHeading = element.p( globalVars.pHeadingLog );
        button   = element.button( globalVars.buttonTextLog, globalVars.buttonClass );

        button.addEventListener( 'click', login );

        signIn.hidden = true;
        signUp.hidden = false;

    } else {

        h2       = element.h2( globalVars.regH2 );
        pHeading = element.p( globalVars.pHeadingReg );
        button   = element.button( globalVars.buttonTextReg, globalVars.buttonClass );

        button.addEventListener( 'click', registration );

        signIn.hidden = false;
        signUp.hidden = true;

    }
    
    const divLoginGroup          = element.inputLabelGroup( globalObject.dataInputLabelLogin );
    const divPassGroup           = element.inputLabelGroup( globalObject.dataInputLabelPass );
    const divShowHidePass        = element.inputLabelGroup( globalObject.dataInputLabelShowHidePass );
    const divRepeatPassGroup     = element.inputLabelGroup( globalObject.dataInputLabelRepeatPass );
    const divSecurityProgressBar = createSecurityProgressBar();

    const bloksForm = [
        h2, 
        pHeading,
        divLoginGroup, 
        divPassGroup, 
        divSecurityProgressBar, 
        divRepeatPassGroup, 
        divShowHidePass, 
        button, 
        pLink
    ];

    if (triggerForm) {
        const repeatPass  = bloksForm.find( (item) => item == divRepeatPassGroup );
        const progressBar = bloksForm.find( (item) => item == divSecurityProgressBar );
        const linkToLogin = bloksForm.find( (item) => item == pLink );

        repeatPass.hidden  = true;
        progressBar.hidden = true;
        linkToLogin.hidden = true;
    }

    for(let elementOfForm of bloksForm){
        form.append( elementOfForm );
    }

    divIdForm.append( form );
    formWrapper.append( divIdForm );

    globalVars.root.append( formWrapper );

    linkListener();
    showPass();
    indicationPass();
}