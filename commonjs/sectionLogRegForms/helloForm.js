import {CreateElements} from '../globalFunctions.js';
import {root}           from '../globalVariables.js';
import {exitFunction}   from './exitFunction.js';


export function createHelloForm( name, typeOfForm ) {
    
    const elements = new CreateElements();

    const logOut = document.querySelector('.user-nav__item:last-child');

    const helloForm  = elements.div('hello-form');
    const userName   = elements.p( name, 'userNameHelloForm' );
    let imgHelloForm = elements.div('imgHelloForm');

    let formText, headMonkey;

    if (typeOfForm == 'reg') {
        
        formText   = elements.p('congratulations mr / mrs: ', 'form-text');
        headMonkey = elements.p( 'you have been successfully registered', 'headMonkey' );
        imgHelloForm.innerHTML = '<i class="fas fa-check"></i>';

    } else {

        formText   = elements.p('Welcome mr / mrs: ', 'form-text');
        headMonkey = elements.p( 'Press on head of monkey in order to play', 'headMonkey' );
        imgHelloForm.innerHTML = '<i class="fab fa-mailchimp"></i>';

        imgHelloForm.addEventListener('click', () => {
            window.location.href = './game.html';
        });

    }

    helloForm.append( formText, userName, imgHelloForm, headMonkey );

    logOut.addEventListener('click', exitFunction);

    root.append( helloForm );

}
