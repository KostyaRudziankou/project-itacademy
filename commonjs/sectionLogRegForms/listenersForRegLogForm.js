
export function listenersLogRegForm( paramsForListenersForm ) {

    const {

        inputName, 
        inputPass, 
        labelName, 
        labelPass,
        repeatPass, 
        labelRepeat, 

    } = paramsForListenersForm;

    inputName.addEventListener('focus', () => {
        inputName.style.boxShadow = "orange 0px 0px 3px";
        labelName.textContent = 'Username';
        labelName.style = 'color: gray; font-size: 14px;'; 
    });

    inputName.addEventListener('blur', () => {
        inputName.style.boxShadow = "none";
        labelName.style = 'color: gray;'; 
    });

    inputPass.addEventListener('focus', () => {
        inputPass.style.boxShadow = "orange 0px 0px 3px";
        labelPass.textContent = 'Password';
        labelPass.style = 'color: gray; font-size: 14px;'; 
    });

    inputPass.addEventListener('blur', () => {
        inputPass.style.boxShadow = "none";
        labelPass.style = 'color: gray;'; 
    });

    if ( repeatPass && labelRepeat ) {

        repeatPass.addEventListener('focus', () => {
            repeatPass.style.boxShadow = "orange 0px 0px 3px";
            labelRepeat.textContent = 'Password';
            labelRepeat.style = 'color: gray; font-size: 14px;'; 
        });

        repeatPass.addEventListener('blur', () => {
            repeatPass.style.boxShadow = "none";
            labelRepeat.style = 'color: gray;'; 
        }); 
        
    }

}