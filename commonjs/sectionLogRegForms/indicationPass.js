
export function indicationPass() {

    const inputPass              = document.querySelector('.input-password');
    let   divSecurityProgressBar = document.querySelector('.security-progress-bar');

    function progressBarForPass() {
        let lengthOfPass = inputPass.value.length;

        if (divSecurityProgressBar) {
            divSecurityProgressBar.style.width = lengthOfPass * 10 + '%';
            if (lengthOfPass <= 5) {
            divSecurityProgressBar.style.backgroundColor = 'red';
            } else if (lengthOfPass > 5 && lengthOfPass < 8) {
            divSecurityProgressBar.style.backgroundColor = 'orange';
            } else {
            divSecurityProgressBar.style.backgroundColor = 'green';
            }
        } else divSecurityProgressBar = null;
    }

    inputPass.addEventListener('input', progressBarForPass);
}