import {inputLabelForWrongData} from '../globalFunctions.js';
import {textLabelsLog}          from '../globalVariables.js';
import {createHelloForm}        from './helloForm.js';
import {listenersLogRegForm}    from './listenersForRegLogForm.js';

export function login(event) {

    event.preventDefault();
    
    const inputName   = document.querySelector('.input-login');
    const inputPass   = document.querySelector('.input-password');
    const labelName   = document.querySelector('.label-login');
    const labelPass   = document.querySelector('.label-pass');
    const formWrapper = document.querySelector('.log-reg-form__wrapper');

    const signIn = document.querySelector('.user-nav__item:nth-child(1)');
    const signUp = document.querySelector('.user-nav__item:nth-child(2)');
    const logOut = document.querySelector('.user-nav__item:last-child');

    const paramForListenersForm = { inputName, inputPass, labelName, labelPass, };

    const user = JSON.parse(localStorage.getItem( inputName.value ));

    const [ 
        
        emptyField, 
        userNotFound, 
        wrongPass,

    ] = textLabelsLog;

    if (inputName.value === '') {
    
        inputLabelForWrongData( inputName, labelName, emptyField );      

    } else if (user == null) {
        
        inputLabelForWrongData( inputName, labelName, userNotFound );

    } else if (inputPass.value != user.pass) {

        inputLabelForWrongData( inputPass, labelPass, wrongPass );

    } else {
        
        formWrapper.remove();
        
        createHelloForm(user.name);
        
        localStorage.setItem( 'user', JSON.stringify(user) );

        signIn.hidden = true;
        signUp.hidden = true;
        logOut.hidden = false;

        return user;
    }

    // EventListeners
    listenersLogRegForm( paramForListenersForm );

}