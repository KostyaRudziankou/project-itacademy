
export function exitFunction() {

    localStorage.setItem('user', JSON.stringify(null));

    window.location.href = './index.html';

}