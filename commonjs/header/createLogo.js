
import { CreateElements }  from '../globalFunctions.js';
import {aClassLogo, logoSrc, logoAlt, logoWidth} from '../globalVariables.js';

export const createLogo = (classLogo = aClassLogo) => {
    
    const elements = new CreateElements();

    const a   = elements.a('', '', classLogo);
    const img = elements.img(logoSrc, logoAlt, logoWidth);

    a.append( img );

    return a;
}