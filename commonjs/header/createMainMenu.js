import {createUl} from './createUl.js';

export function createMainMenu() {
    const divMenu     = document.createElement('div');
    const divMenuWrap = document.createElement('div');

    divMenu.classList     = 'menu';
    divMenuWrap.classList = 'menu__wrap';

    const itemsForMainMenu = ['Main page', 'About me'];
    const classUlMainMenu  = 'menu__main';
    const objForLiAndLinkForMainMenu = {
        liClassName: 'menu__main-item',
        aClassName: 'menu__main-link',
        aHref: '#',
      }

    divMenuWrap.append(createUl( itemsForMainMenu, classUlMainMenu, objForLiAndLinkForMainMenu ));
    divMenu.append( divMenuWrap );

    return divMenu;
}