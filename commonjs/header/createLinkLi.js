// import {globalVariables as global} from '../globalVariables.js';

export function createLinkLi(nameItemMenu = 'li', objForLiAndLinkForMainMenu) { 

    const li = document.createElement('li');
    const a  = document.createElement('a');
    
    a.innerHTML = nameItemMenu;
    a.setAttribute('href', objForLiAndLinkForMainMenu.aHref);
    a.classList  = objForLiAndLinkForMainMenu.aClassName;
    
    li.classList = objForLiAndLinkForMainMenu.liClassName;
    li.append( a );

    return li;
}