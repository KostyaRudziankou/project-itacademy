import {createLinkLi} from './createLinkLi.js';

export function createUl( nameItemMenu, classUl, objForLiAndLinkForMainMenu ) {

    const ul = document.createElement('ul');

    ul.classList = classUl;

    for (let data of nameItemMenu) {

        const li = createLinkLi( data, objForLiAndLinkForMainMenu );

        ul.append( li );
    }

    return ul;
}