import {createUl} from './createUl.js';
import {createMainMenu} from './createMainMenu.js';
import {createLogo} from './createLogo.js';

export function createHeader() {

    const header                 = document.createElement('header');
    const divPageHeaderWrap      = document.createElement('div');
    const divPageHeaderContainer = document.createElement('div');

    header.classList.add('page-header');
    divPageHeaderWrap.classList.add('page-header__wrapper');
    divPageHeaderContainer.classList.add('page-header__container');

    // create user-nav
    const itemsForUserNav = [
      '<i class="fas fa-user-plus"></i>',
      '<i class="fas fa-sign-in-alt"></i>',
      '<i class="fas fa-sign-out-alt"></i>',
    ];

    const classUlUserNav  = 'user-nav';
    const objForLiAndLinkForUserNav = {
        liClassName: 'user-nav__item',
        aClassName: 'user-nav__link',
        aHref: '#',
      }

    const userNav = createUl( itemsForUserNav, classUlUserNav, objForLiAndLinkForUserNav );


    divPageHeaderContainer.append( createLogo() );
    divPageHeaderContainer.append( createMainMenu() );
    divPageHeaderContainer.append( userNav );
    divPageHeaderWrap.append( divPageHeaderContainer );
    header.append( divPageHeaderWrap );

    return header;
}