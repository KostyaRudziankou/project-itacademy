'use strict';
import {root}              from './commonjs/globalVariables.js'; 
import {createHeader}      from './commonjs/header/createHeader.js';
import {createFormWrapper} from './commonjs/sectionLogRegForms/createLogRegForms.js';
import {createHelloForm}   from './commonjs/sectionLogRegForms/helloForm.js';

root.prepend( createHeader() );

const signIn      = document.querySelector('.user-nav__item:nth-child(2)');
const signUp      = document.querySelector('.user-nav__item:nth-child(1)');

// check log user
const user = JSON.parse( localStorage.getItem('user') );

if (user == null) {
    
    const logOut = document.querySelector('.user-nav__item:last-child');
    
    logOut.hidden = true;
    
    createFormWrapper();
    
} else {
    
    createHelloForm( user.name );

    signIn.hidden = true;
    signUp.hidden = true;

}

signIn.addEventListener('click', () => {
    
    const formWrapper = document.querySelector('.log-reg-form__wrapper');

    const logForm = createFormWrapper.bind(this, true);
    
    formWrapper.remove();

    logForm();

})

signUp.addEventListener('click', () => {
    
    const formWrapper = document.querySelector('.log-reg-form__wrapper');

    const regForm = createFormWrapper.bind( this, false );
    
    formWrapper.remove();

    regForm();

})